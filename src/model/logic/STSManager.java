package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStop_times;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager 
{

	@Override
	public void loadRoutes(String routesFile) {


		DoubleLinkedList<VORoute> routes=new DoubleLinkedList<VORoute>();
		
		
		String csvFile = routesFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			
			while (line  != null) 
			{   
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				//System.out.println(datos[0] + ", " + datos[1] + ", " + datos[2] + ", " + datos[3] + ", " + datos[4] + ", " + datos[5]+ ", " + datos[6]+ ", " + datos[7]+ ", " + datos[8]);
				VORoute v = new VORoute(datos[0] ,  datos[1], datos[2],  datos[3] , datos[4] , datos[5],  datos[6],  datos[7] ,  datos[8]);
				
				routes.add(v);
				line =br.readLine();
			}
			br.close();
			System.out.println("" +routes.getSize());
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		// TODO Auto-generated method stub

	}

	@Override
	public void loadTrips(String tripsFile) 
	{
		int contador = 0;
		RingList<VOTrip> routes2= new RingList<VOTrip>();
		String csvFile = "data/trips.txt";
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			while (line  != null) 
			{                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				//System.out.println(datos[0] + ", " + datos[1] + ", " + datos[2] + ", " + datos[3] + ", " + datos[4] + ", " + datos[5]+ ", " + datos[6]+ ", " + datos[7]+ ", " + datos[8]+ ", " + datos[9]);
				VOTrip v = new VOTrip(datos[0] ,  datos[1], datos[2],  datos[3] , datos[4] , datos[5],  datos[6],  datos[7] ,  datos[8],datos[9]);
				routes2.add(v);
				line =br.readLine();
				contador ++;
			}
			br.close();
			System.out.println("" +routes2.getSize());
			System.out.println(contador);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// TODO Auto-generated method stub

	}

	@Override
	public void loadStopTimes(String stopTimesFile) 
	{
		
		DoubleLinkedList<VOStop_times> routes3=new DoubleLinkedList<VOStop_times>();
		
		String csvFile = "data/stop_times.txt";
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			while (line  != null) 
			{                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				
				String d1=datos[0];
				String d2=datos[1];
				String d3=datos[2];
				String d4=datos[3];
				String d5=datos[4];
				String d6=datos[5];
				String d7=datos[6];
				String d8=datos[7];
				String d9="";
				if(datos.length==8)
				{
				 d9="";
				}
				else
				{
				 d9=datos[8];
				}
		//	System.out.println(d1 + ", " + d2 + ", " + d3 + ", " + d4 + ", " + d5+ ", " + d6+ ", " + d7+ ", " + d8+ ", " + d9);
				VOStop_times v = new VOStop_times(d1 ,  d2, d3,  d4 , d5 , d6,  d7,  d8 ,  d9);
				routes3.add(v);
				line =br.readLine();
			}
			br.close();
			System.out.println("" +routes3.getSize());
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		// TODO Auto-generated method stub

	}




	@Override
	public void loadStops(String stopsFile) 
	{
		RingList<VOStop> routes3=new RingList<VOStop>();
		String csvFile = "data/stops.txt";
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			while (line  != null) 
			{                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.

				VOStop v = new VOStop(datos[0] ,  datos[1], datos[2],  datos[3] , datos[4] , datos[5],  datos[6],  datos[7] ,  datos[8], "");
				//System.out.println(datos[0] + ", " + datos[1] + ", " + datos[2] + ", " + datos[3] + ", " + datos[4] + ", " + datos[5]+ ", " + datos[6]+ ", " + datos[7]+ ", " + datos[8]+ ", " + "");
				routes3.add(v);
				line =br.readLine();
				
			}
			br.close();
			System.out.println("" +routes3.getSize());
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		// TODO Auto-generated method stub

	}



	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
