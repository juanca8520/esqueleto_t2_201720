package model.vo;

import model.data_structures.Identificador;

public class VOStop_times implements Identificador
{
private String tripid;
private String arrival;
private String departure;
private String stopid;
private String sequence;
private String stopheadsign;
private String pickup;
private String drop;
private String shape;

public VOStop_times(String pTripid, String pArrival,String pDeparture, String pStopid,String pSequence,String pStopheadsign, String pPickup,String pDrop, String pShape)
{
tripid=pTripid;
arrival=pArrival;
departure=pDeparture;
stopid=pStopid;
sequence=pSequence;
stopheadsign=pStopheadsign;
pickup=pPickup;
drop=pDrop;
shape=pShape;
}

public String darTripid()
{
	return tripid;
}
public String darArrival()
{
	return arrival;
}
public String darDeparture()
{
	return departure;
}
public String darStopid()
{
	return stopid;
}
public String darSequence()
{
	return sequence;
}
public String darStopheadsign()
{
	return stopheadsign;
}
public String darPickup()
{
	return pickup;
}
public String darDrop()
{
	return drop;
}
public String darShape()
{
	return shape;
}

@Override
public String darIdentificador() {
	// TODO Auto-generated method stub
	return tripid;
}
}
