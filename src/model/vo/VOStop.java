package model.vo;

import model.data_structures.Identificador;

/**
 * Representation of a Stop object
 */

public class VOStop 
{

	private String id;
	private String code;
	private String name;
	private String description;
	private String latitud;
	private String longitud;
	private String zonaid;
	private String url;
	private String tipo;
	private String station;
	
	public VOStop (String pId, String pCode, String pName,String pDescription, String pLatitud,String pLongitud,String pZonaid, String pUrl, String pTipo, String pStation)
	{
		id=pId;
		code=pCode;
		name=pName;
		description=pDescription;
		latitud=pLatitud;
		longitud=pLongitud;
		zonaid=pZonaid;
		url=pUrl;
		tipo=pUrl;
		station=pStation;
	}

	public String darId()
	{
		return id;
	}
	public String darCode()
	{
		return code;
	}
	public String darName()
	{
		return name;
	}
	public String darDescription()
	{
		return description;
	}
	public String darLatitud()
	{
		return latitud;
	}
	public String darLongitud()
	{
		return longitud;
	}
	public String darZonaid()
	{
		return zonaid;
	}
	public String darUrl()
	{
		return url;
	}
	public String darTipo()
	{
		return tipo;
	}
	public String darStation()
	{
		return station;
	}

	


}
