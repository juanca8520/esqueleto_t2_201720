package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Identificador;

/**
 * Representation of a route object
 */
public class VORoute <T extends Identificador>
{
	private String routeid;
	private String agencyid;
	private String shortname;
	private String longname;
	private String description;
	private String tipo;
	private String url;
	private String color;
	private String colortexto;
//	private DoubleLinkedList<VOTrip>trips;
	
	public VORoute(String pRouteid,String pAgencyid, String pShortname, String pLongname, String pDescription, String pTipo, String pUrl,String pColor,String pColortexto)
	{
		routeid=pRouteid;
		agencyid=pAgencyid;
		shortname=pShortname;
		longname=pLongname;
		description=pDescription;
		tipo=pTipo;
		url=pUrl;
		color=pColor;
		colortexto=pColortexto;
	}
	

	/**
	 * @return id - Route's id number
	 */
	public String darRouteid()
	{
		return routeid;
	}

	public String darAgencyid()
	{
		return agencyid;
	}
	public String darShortname()
	{
		return shortname;
	}
	public String darLongname()
	{
		return longname;
	}
	public String darDescription()
	{
		return description;
	}
	public String darTipo()
	{
		return tipo;
	}
	public String darUrl()
	{
		return url;
	}

	public String darColor()
	{
		return color;
	}

	public String darColortexto()
	{
		return colortexto;
	}


	
	}
