package model.vo;

import model.data_structures.Identificador;

public class VOTrip 
{
private String routeid;
private String serviceid;
private String tripid;
private String headsign;
private String shortname;
private String direction;
private String block;
private String shape;
private String wheelchair;
private String bikes;

public VOTrip(String pRouteid, String pServiceid, String pTripid,String pHeadsign, String pShortname,String pDirection,String pBlock, String pShape, String pWheelchair, String pBikes)
{
	routeid=pRouteid;
	serviceid=pServiceid;
	tripid=pTripid;
	headsign=pHeadsign;
	shortname=pShortname;
	direction=pDirection;
	block=pBlock;
	shape=pShape;
	wheelchair=pWheelchair;
	bikes=pBikes;
}

public String darRouteid()
{
	return routeid; 
}
public String darServiceid()
{
	return serviceid;
}
public String darTripid()
{
	return tripid;		
}
public String darHeadsign()
{
	return headsign;
}
public String darShortname()
{
	return shortname; 
}
public String darDirection()
{
	return direction;
}
public String darBlock()
{
	return block;
}
public String darShape()
{
	return shape;
}
public String darWheelchair()
{
	return wheelchair;
}
public String darBikes()
{
	return bikes;
}


}
