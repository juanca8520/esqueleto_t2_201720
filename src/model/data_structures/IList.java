package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> 
{

	int getSize();
	
	void add(T elem);
	
	void addAtEnd(T elem);
	
	void addAtK(int n, T elem);
	
	T getElement(int n);
	
	T getCurrentElement();
	
	void delete(T elem);
	
	T deleteAtK( int n);
	
	void next();
	
	void previous();

	

}
