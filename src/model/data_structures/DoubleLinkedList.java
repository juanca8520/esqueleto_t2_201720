package model.data_structures;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ListIterator;

public class DoubleLinkedList <T > extends AbstractList<T> 
{

	private static final long serialVersionUID = 1L;

	Node<T> primero;

	Node<T> anterior;

	Node<T> actual;

	public DoubleLinkedList()
	{
		primero = null;
		actual = primero;
	}

	public DoubleLinkedList(T nPrimero)
	{
		if(nPrimero == null)
		{
			throw new NullPointerException();
		}
		primero = new Node<T>(nPrimero);
		actual = primero;
	}

	private class Iterator<T > implements ListIterator<T>
	{
		private Node<T> actual;

		public Iterator(Node<T> nActual)
		{
			actual = nActual;
		}


		public boolean hasNext()
		{
			if(actual.darSiguiente()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public boolean hasPrevious()
		{
			if(actual.darAnterior()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public T next()
		{
			if(actual.darSiguiente()==null)
			{
				throw new IndexOutOfBoundsException();
			}
			else{
				return actual.darSiguiente().darElemento();

			}
		}

		public T previous()
		{
			if(actual.darAnterior() == null)
			{
				throw new IndexOutOfBoundsException();
			}
			else {
				return actual.darAnterior().darElemento();

			}
		}


		@Override
		public void add(T e)
		{

			// TODO Auto-generated method stub

		}


		@Override
		public int nextIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public int previousIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}


		@Override
		public void set(T e) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>(actual);
		// TODO Auto-generated method stub
	}

	@Override
	public int getSize() 
	{
		int size = 0;
		Node<T> nodo = primero;

		while (nodo != null)
		{
			size++;
			nodo = nodo.darSiguiente();
		}
		// TODO Auto-generated method stub
		return size;
	}

		@Override
		public void add(T elem) 
		{
			Node<T> agregar = new Node<T>((T) elem);
			boolean agrego = false;
			if(elem==null)
			{
				throw new NullPointerException();
			}
			else if(primero == null)
			{
				primero = new Node<T>((T) elem);
			}
			else
			{
				Node<T> temporal = primero;
				primero = agregar;
				primero.cambiarSiguiente(temporal);
				temporal.cambiarAnterior(primero);
			}
			// TODO Auto-generated method stub
	
		}

	@Override
	public void addAtEnd(T elem)
	{
		Node<T> agregar = new Node<T>((T) elem); 
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
		}
		else
		{
			Node<T> temporal = primero;
			while(temporal.darSiguiente()!=null)
			{
				if(temporal.darSiguiente()==null)
				{
					temporal.cambiarSiguiente(agregar);
				}
				else 
				{
					temporal = temporal.darSiguiente();
				}
			}
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void addAtK(int n, T elem)
	{
		int cont = 0;
		Node<T> agregar = new Node<T>((T) elem);
		boolean add = false;
		if(elem==null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
		}
		else 
		{
			Node<T> temporal = primero;

			while (temporal.darSiguiente()!=null )
			{
				if(cont + 1 == n)
				{
					agregar.cambiarSiguiente(temporal.darSiguiente());
					temporal.darSiguiente().cambiarAnterior(agregar);
					temporal.cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal);
					//					anterior.cambiarSiguiente(agregar);
					//					agregar.cambiarSiguiente(temporal);
					//					agregar.cambiarAnterior(temporal2);
				}
				else if(cont == n||temporal.darAnterior()==null)
				{
					agregar.cambiarSiguiente(primero);
					primero.cambiarAnterior(agregar);
					primero = agregar;
				}
				else if(n==getSize())
				{
					addAtEnd(elem);
				}
				cont++;
				temporal = temporal.darSiguiente();
			}
		}
		// TODO Auto-generated method stub

	}

	@Override
	public T getElement(int n) 
	{
		Node<T> temporal = primero;
		Node<T> encontrado = null;

		boolean encontro = false;
		int cont = 0;
		while(temporal!=null&&!encontro)
		{
			if(cont == n)
			{
				encontrado = temporal;
				encontro = true;
			}
			cont ++;
			temporal = temporal.darSiguiente();
		}

		return encontrado.darElemento();
		// TODO Auto-generated method stub
	}




	@Override
	public void delete(T elem) 
	{
		Node<T> temporal = primero;
		boolean delete = false;

		while (temporal != null && !delete)
		{
			if(temporal.darElemento().equals(elem)&&temporal.darAnterior()==null)
			{
				primero = temporal.darSiguiente();
				delete = true;
			}
			else if(temporal.darElemento().equals(elem))
			{
				temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
				temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
				delete = true;
			}
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub

	}

	@Override
	public T deleteAtK(int n)
	{
		Node<T> temporal = primero;
		Node<T> eliminar = null;
		int cont = 0;
		boolean elimino = false;

		if(n<0 || n>getSize())
		{
			throw new IndexOutOfBoundsException();
		}
		else
		{
			while(temporal != null && !elimino)
			{
				if(cont == n&&temporal.darAnterior()==null)
				{
					primero = temporal.darSiguiente();
					elimino = true;
				}
				else if (cont == n)
				{
					eliminar = temporal;
					temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
					temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
					elimino = true;
				}
				cont++;
			}
		}
		// TODO Auto-generated method stub
		return temporal.darElemento();

	}

	

//	@Override
//	public void add(T elem) 
//	{
//		boolean agrego = false;
//		Node<T> temporal = primero;
//		Node<T> agregar = new Node<T>(elem);
//		if(elem == null)
//		{
//			throw new NullPointerException();
//		}
//
//		else {
//			if (primero == null)
//			{
//				primero = agregar;
//			}
//			else
//			{
//				while (temporal.darSiguiente()!=null && !agrego)
//				{
//					if(compare(elem, temporal.darElemento())<=0)
//					{
//						if(temporal.darAnterior()==primero)
//						{
//							agregar.cambiarSiguiente(temporal);
//							temporal.cambiarAnterior(agregar);
//							primero = agregar;
//							agrego = true;
//						}
//						else if(temporal.darSiguiente()==null)
//						{
//							agregar.cambiarSiguiente(temporal);
//							temporal.cambiarAnterior(agregar);
//							temporal.darAnterior().cambiarSiguiente(agregar);
//							agregar.cambiarAnterior(temporal.darAnterior());
//							agrego = true;
//						}
//						else {
//							agregar.cambiarSiguiente(temporal);
//							temporal.cambiarAnterior(agregar);
//							temporal.darAnterior().cambiarSiguiente(agregar);
//							agregar.cambiarAnterior(temporal.darAnterior());
//							agrego=true;
//						}
//					}
//					else if (compare(elem, temporal.darElemento())>0)
//					{
//						if(temporal.darSiguiente()==null)
//						{
//							temporal.cambiarSiguiente(agregar);
//							agregar.cambiarAnterior(temporal);
//							agrego=true;
//						}
//						else {
//							agregar.cambiarSiguiente(temporal.darSiguiente());
//							temporal.darSiguiente().cambiarAnterior(agregar);
//							temporal.cambiarSiguiente(agregar);
//							agregar.cambiarAnterior(temporal);
//							agrego=true;
//						}
//					}
//				}
//				temporal = temporal.darSiguiente();
//			}
//		}
//
//		// TODO Auto-generated method stub
//
//	}

//	@Override
//	public int compare(T o1, T o2)
//	{
//		int compare = o1.darIdentificador().compareTo(o2.darIdentificador());
//		if(compare > 0)
//		{
//			return 1;
//		}
//		else if(compare < 0)
//		{
//			return -1;
//		}
//		else
//		{
//			return 0;
//		}
//		// TODO Auto-generated method stub
//	}



}