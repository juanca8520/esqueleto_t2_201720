package model.data_structures;

public abstract class AbstractList<T > implements IList<T>
{
    private static final long serialVersionUID = 1L;

	protected Node<T> primero;

	protected Node<T> actual;

	public void clear()
	{
		primero = null;
	}
	
//	public Object[] toArray()
//	{
//		
//	}

	public boolean isEmpty()
	{
		if(primero == null)
		{
			return true;
		}
		else {
			return false;
		}
	}
	public T getCurrentElement()
	{
		return actual.darElemento();
	}

	public void next()
	{
		if(actual.darSiguiente()!=null)
		{
			actual = actual.darSiguiente();
		}
	}

	public void previous()
	{
		if(actual.darAnterior()!=null)
		{
			actual = actual.darAnterior();
		}
	}

}
