package model.data_structures;

import java.io.Serializable;

public class Node <T > implements Serializable 
{

    private static final long serialVersionUID = 1L;

	protected T elemento;

	protected Node<T> siguiente;

	protected Node<T> anterior;

	public Node(T nElemento)
	{
		elemento = nElemento;
		siguiente = null;
		anterior = null;
	}

	public void cambiarSiguiente (Node<T> nSiguiente)
	{
		siguiente = nSiguiente;
	}

	public Node<T> darSiguiente()
	{
		return siguiente;
	}

	public Node<T> darAnterior()
	{
		return anterior;
	}

	public void cambiarAnterior (Node<T> pNodo)
	{
		anterior = pNodo;
	}

	public T darElemento()
	{
		return elemento;
	}

	public void cambiarElemento (T pElemento)
	{
		elemento = pElemento;
	}

	



}
