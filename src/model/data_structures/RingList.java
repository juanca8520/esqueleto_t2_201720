package model.data_structures;

import java.util.Iterator;
import java.util.ListIterator;


public class RingList <T> extends AbstractList  


{
	private static final long serialVersionUID = 1L;

	Node<T>primero;
	Node<T>anterior;
	Node<T>ultimo;
	Node<T>actual;

	public RingList()
	{
		primero = null;
		actual = null;
		ultimo = null;
	}

	public RingList(T nElem)
	{
		if(nElem == null)
		{
			throw new NullPointerException();
		}
		primero = new Node<T>(nElem);
		actual = primero;
		ultimo = primero;

	}

	private class Iterator<T > implements ListIterator<T>
	{
		private Node<T> actual;

		public Iterator(Node<T> nActual)
		{
			actual = nActual;
		}

		public boolean hasNext()
		{
			if(actual.darSiguiente()!=null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public boolean hasPrevious()
		{
			if(actual.darAnterior()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		@Override
		public void add(T e) {
			// TODO Auto-generated method stub

		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int nextIndex() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public T previous() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int previousIndex() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}

		@Override
		public void set(T e) {
			// TODO Auto-generated method stub

		}

	}


	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>(actual);
		// TODO Auto-generated method stub
	}

	@Override
	public int getSize() 
	{

		//Node<T> referencia = primero;
		Node<T> temporal = primero;
		Node<T> ref = primero;
		int size = 0;
		boolean termino = false;
		while(temporal.darSiguiente()!=null && !termino) 
		{
			if(temporal.darSiguiente().equals(ref))
			{
				termino = true;
			}
			size++;
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void add(Object elem)
	{
		Node<T> agregar = new Node<T>((T) elem);
		Node<T> temporal = primero;
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar; 
			ultimo = agregar;
			primero.cambiarSiguiente(ultimo);
			ultimo.cambiarAnterior(primero);
			primero.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(primero);
		}
		else if(primero!= null && primero.darSiguiente()==null)
		{
			//primero.cambiarSiguiente(nSiguiente);
			
//			agregar.cambiarSiguiente(primero);
//			agregar.cambiarAnterior(primero);
//			primero.cambiarSiguiente(agregar);
//			primero.cambiarAnterior(agregar);
//			primero = agregar;
//			ultimo = primero;
		}
		else {
			ultimo.cambiarSiguiente(agregar);
			agregar.cambiarAnterior(ultimo);
			agregar.cambiarSiguiente(primero);
			primero.cambiarAnterior(agregar);
			primero = agregar;

		}


		// TODO Auto-generated method stub

	}

	@Override
	public void addAtEnd(Object elem)
	{
		Node<T>agregar = new Node<T>((T) elem);

		if(elem==null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
			actual = primero;
			ultimo = primero;
		}
		else if(primero != null && primero.darSiguiente()== null)
		{
			primero.cambiarSiguiente(agregar);
			agregar.cambiarAnterior(primero);
			primero.cambiarAnterior(agregar);
			agregar.cambiarSiguiente(primero);
			ultimo = agregar;
		}
		else {
			ultimo.cambiarSiguiente(agregar);
			agregar.cambiarAnterior(ultimo);
			agregar.cambiarSiguiente(primero);
			primero.cambiarAnterior(agregar);
			ultimo = agregar;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void addAtK(int n, Object elem)
	{
		int cont = 0;
		Node<T>agregar = new Node<T>((T) elem);
		Node<T> temporal = primero;
		boolean agrego = false;
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else if (primero == null)
		{
			primero = agregar;
			actual = primero;
			ultimo = primero;
		}
		else {


			while(temporal.darSiguiente()!=primero&&!agrego)
			{
				if(cont + 1 == n)
				{
					agregar.cambiarSiguiente(temporal.darSiguiente());
					temporal.darSiguiente().cambiarAnterior(agregar);
					temporal.cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal);
				}
				cont++;
				temporal = temporal.darSiguiente();
			}	
		}

		// TODO Auto-generated method stub

	}

	@Override
	public Object getElement(int n) 
	{
		int cont = 0;
		Node<T> temporal = primero;
		boolean encontro = false;
		T elem = null;
		while(temporal.darSiguiente()!=primero && !encontro)	
		{
			if(cont == n)
			{
				elem = temporal.darElemento();
				encontro = true;
			}
			cont++;
			temporal = temporal.darSiguiente();
		}
		return elem;
		// TODO Auto-generated method stub
	}



	@Override
	public void delete(Object elem)
	{
		boolean elimino = false;
		Node<T>temporal = primero;
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else {
			while (temporal.darSiguiente()!=primero && !elimino)
			{
				if(temporal.darElemento().equals(elem))
				{
					temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
					temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
					elimino = true;
				}
				temporal = temporal.darSiguiente();
			}
		}
		// TODO Auto-generated method stub

	}

	@Override
	public T deleteAtK( int n) 
	{
		int cont = 0;
		Node<T>temporal = primero;

		Node<T>eliminar = null;
		boolean elimino = false;

		//		if(elem == null)
		//		{
		//			throw new NullPointerException();
		//		}


		while(temporal.darSiguiente()!= primero && !elimino)
		{
			if(cont == n)
			{
				eliminar = temporal.darSiguiente();
				temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
				temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
				elimino = true;
			}
			cont++;
			temporal = temporal.darSiguiente();
		}
		//System.out.println(eliminar.darElemento());
		return eliminar.darElemento();

		// TODO Auto-generated method stub

	}

	//public void agregarOrdenado()


}
