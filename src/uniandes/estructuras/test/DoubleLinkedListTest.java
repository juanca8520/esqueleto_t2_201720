package uniandes.estructuras.test;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Identificador;
import model.data_structures.Node;
import model.vo.VOStop;

public class DoubleLinkedListTest<T > extends TestCase
{

	private DoubleLinkedList<T> lista;
	private String cadena;
	private String cadena2;
	private String cadena3;

	public void setUp()
	{
		
		lista = new DoubleLinkedList<T>();
		lista.add ((T) "1");
		lista.add((T) "2");
		lista.add((T) "4");
		lista.add((T) "5");

	}

	public void testSize()
	{

		assertEquals("La lista no tiene el tama�o esperado", 4, lista.getSize()  );
	}
	public void testAddAtk()
	{
		lista.addAtK(4, (T) "100");
		//		for (int i = 0; i<lista.getSize(); i++)
		//		{
		//					System.out.println(lista.getElement(i));
		//		}
		assertEquals("La lista no est� a�adiendo elementos en el indice indicado", "100", lista.getElement(0));
	}

	public void testEliminarAtK()
	{
		lista.deleteAtK(2);
//		for (int i = 0; i<lista.getSize(); i++)
//		{
//			System.out.println(lista.getElement(i));
//		}
		assertEquals("error", "4", lista.deleteAtK(2));


	}

}
