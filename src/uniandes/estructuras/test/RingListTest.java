package uniandes.estructuras.test;

import junit.framework.TestCase;
import model.data_structures.Identificador;
import model.data_structures.RingList;

public class RingListTest<T> extends TestCase
{
	
	private RingList<T> lista;
	
	public void setUp()
	{
		lista = new RingList<T>();
		lista.add("uno");
		lista.add("dos");
		lista.add("tres");
		lista.add("cuatro");
	}
	
	public void testSize()
	{
		for (int i = 0; i < lista.getSize(); i++) 
		{
			System.out.println(lista.getElement(i));
		}
		assertEquals("Error" , 4, lista.getSize());
	}
	
	public void testAddAtk()
	{
		lista.addAtK(3, "agregar");
		
		assertEquals("Error", "agregar", lista.getElement(3));
	}
	
	public void testEliminarAtK()
	{
		//System.out.println(lista.deleteAtK(2));
		lista.deleteAtK(2);
		assertEquals("Error", "dos", lista.deleteAtK(2));
	}
}
